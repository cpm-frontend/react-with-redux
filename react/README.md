# React

*React* is a famous frontend framework, let's build a simple app to get a taste.

Open your browser and visit [CodeSandbox](https://codesandbox.io/s/react-yud4c), you can see some code there. Replace all code in `App.js` with following code:
```js
import "./styles.css";
import React, { useState } from "react";
import Field from "./components/field";
import Language from "./components/languages";
import Translate from "./components/translate";

export default function App() {
  const [language, setLanguage] = useState("ru");
  const [text, setText] = useState("");

  return (
    <div>
      <Field labe="Enter English" onChange={setText} value={text} />
      <Language language={language} onLanguageChange={setLanguage} />
      <hr />
      <Translate text={text} language={language} />
    </div>
  );
}
```

You can type any word in right section and choose language to translate like so:
<br/><img src="./screenshots/first-code.png" width="300"/><br/>

Here are some takeaways:
- `App()` function  referred to as a React component.
- Components in React has 2 jobs:
    - It produces JSX which tells React what we want to show on the screen to the user.
    - It handles user events.
- JSX elements can tell React to create a normal HTML or to show another React component.
- We can see content in the screen because we call `App()` function and get back JSX, turn it into HTML. Finally put it into the DOM.
    <img src="./screenshots/app-show.png" width="700"/>
- `React` library is called a *reconciler*, it knows how to work with components.
- `ReactDOM` library is called a *renderer*, it knows how to take instructions on what we want to show and turn it into HTML.
- `useState()` is a function for working with React's *state system*. State is used to keep track of data that changes over time. It's used to make React update the HTML on the screen.

## JSX

We are going to create a simple project [jsx](jsx/) to learn JSX and *Babel*. Follow the steps to set up:
1. Download *NodeJS* [here](https://nodejs.org/en/download).
1. Check node version by `node -v`.
1. Install package `create-react-app` globally.
    ```
    npm install -g create-react-app
    ```
1. Generate a react project named `jsx`.
    ```
    npx create-react-app jsx
    ```
1. Go to this project and run the app in the development mode, default port is 3000. If you want to stop the app, type `ctrl + C`.
    ```
    cd jsx
    yarn start
    ```

`create-react-app` package includes *Webpack*, *Babel* and Dev server libraries for us, so we don't need to install them manually.

Please refer to the [jsx/src/index.js](jsx/src/index.js) and learn following concepts.

### Babel
ES2015 (ES6) or later code would not be shipped down directly to users' browsers, because they aren't fully supported. We need to use dependency called *Babel* to take ES2015 or later code to convert to ES5 JS code.

### JSX
JSX is special dialect of JS. It looks like to HTML, but it's not. We can use this language to write some code very similar to HTML in JS for convenient. 

Browsers don't understand JSX code, we need Babel to help us convert JSX to ES5 JS code as well. Visit Babel [here](https://babeljs.io/repl) and paste following code to see what translated JS code is.
```js
// Using JSX
const App = () => {
  return <div>Hi there!</div>;
};
```
```js
// After translated by Babel
const App = () => {
  return React.createElement("div", null, "Hi there!");
};
```

What's different between JSX and HTML?
- Adding **custom stying** to an element uses different syntax.
    ```html
    <!-- HTML -->
    <div style="background-color: blue; color: white;"></div>
    ```
    ```js
    // JSX
    // The outer curly braces can reference JS variable inside of JSX, second one is meant to indicate the JS object
    <div style={{backgroundColor: 'blue', color: 'white'}}></div>
    ```
    > ### *String Conventions*
    > Use double quotes for all of the JSX properties and single quotes everywhere else.
- Adding a **class** to an element uses different syntax.
    ```html
    <!-- HTML -->
    <label class="label">
    ```
    ```js
    // JSX
    // 'class' is a JS keyword
    <label className="label">
    ```
- Only JSX can reference JS variables and functions using curly braces. But you are ***NOT*** allowed to reference JS object as **text** in JSX. You can show JS object as **attribute** on element in JSX.
    ```js
    const getText() {...}
    // You cannot use { style } in JSX, but you can use { style.color }
    const style = { color: 'blue' }

    const App = () => {
      const title = 'this is title'
      return (
        <div>
          <h1>{title}</h1>
          {getText()}
        </div>
      )
    }
    ```

### Module Systems
We can import code from different files. For example:
```js
// Import the React and ReactDOM libraries
import React from 'react';
import ReactDOM from 'react-dom';
```

> ### *Note*
> `import` is ES2015 import statement, `required` is CommonJS import statemnet.

## Components

A react component is a function or class that produces HTML to show the users (using JSX) and handles feedback from the users (using event handlers).

There are 3 important features of component: 
- *Component nesting*: A component can be shown inside of another.
- *Component reusability*: We want to make components that can be easily reused through out application.
- *Component configuration*: We should be able to configure a component when it is created.

Now you can refer to the [components](components/) project to learn following concepts. To set up this project, type the following commands:
```
cd components
yarn
yarn start
```

### Semantic UI
[Semantic UI](https://semantic-ui.com/) is a styling or CSS framework. We will use it on our project styling. The easiest way to install it is using CDN. You can visit [Semantic UI CDN](https://cdnjs.com/libraries/semantic-ui) and copy following URL:

```
https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css
```

Then paste it to `head` tag in `index.html` like so:
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    ...
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css" />
    ...
  </head>
  <body></body>
</html>
```

Visit your browser localhost:3000 again, type `option + command + I` and change tab to **Network**, you will see Semantic UI is loaded.
<br /><img src="./screenshots/semantic-css.png" width=550px /><br />

### Faker JS
To generate fake image, we will use *faker.js*, you can visit github [here](https://github.com/marak/Faker.js/). Use `yarn add faker` to install. You can call API like so:
```js
import faker from 'faker';

const App = () => {
  return (
    <div><img alt="avatar" src={faker.image.image()} /></div>
  );
};
```

### Reusable Component
If we want to show some content very similar, it's bad idea to copy and paste. Because it's hard to maintain. We should always reuse the code, here are our steps:
1. Identify the JSX that appears to be duplicated and make a component for those code.
1. Create a new file to house this new component, it should have the same name as the component.
1. Create a new component in the new file, paste the JSX into it.
1. Make the new component configurable by using React's *props system*.

### Component Nesting
So we can create a new file called [CommentDetail](components/src/CommentDetail.js) to house duplicated code in there. Before we use it outside of this file, we have to export the code.
```js
const CommentDetail = () => {...}

// Export the code
export default CommentDetail;
```
Then the certain JSX planning to use that component should import the component (don't need to add `.js`) and use `<>` to show.

```js
import CommentDetail from './CommentDetail';

const App = () => {
  return (
    <div className="ui container comments">
      < CommentDetail />
    </div>
  );
};
```

### Props System
*Props system* (props stands for properties) is a system for passing data from a parent component to a child component or nesting component. Its goal is to customize or configure a child component.

For example, the parent component looks like:

```js
< CommentDetail author="Sam" timeAgo="Today at 4:45PM" avatar={faker.image.image()} />
```

In above code, `author` is name of the prop and `Sam` is value of the prop. You can pass string, number, or function as props to child component, then the child component can consume those props like so:
```js
const CommentDetail = props => {
  return (
    ...
    <img alt="avatar" src={props.avatar} />
    ...
    <a href="/" className="author">{props.author}</a>
    ...
  )
}
```

We can also pass a component as a prop to another component. The following codes show the component `CommentDetail` passed to `ApprovalCard` as a prop:
```jsx
<ApprovalCard>
  <CommentDetail author="Sam" timeAgo="Today at 4:45PM" content="I'm fucked!" avatar={faker.image.avatar()} />
</ApprovalCard>
```

When we want to use this component in `ApprovalCard`, we can call it using `props.children`.
```js
const ApprovalCard = props => {
  return (
    ...
    <div className="content">{props.children}</div>
    ...
  );
};
```

You can also pass string or anything else to component using open tag. And you can call them by using `props.children` as well.
```jsx
<ApprovalCard>
  <div>>Are you sure you want to do this?</div>
</ApprovalCard>
```

## Class Components
There are 2 types of components, i.e. functional components and class components. The following comparison table is how React **used to be**:

Class Components|Functional Components
--|--
It can produce JSX to show content to the user|It can produce JSX to show content to the user
It can use *lifecycle method system* to run code at specific points in time.|
It can use the *state system* to update content on the screen.|

But now, with *hooks system*, React team have given many capabilities to function components:

Class Components|Function Components with Hooks System
--|--
It can produce JSX to show content to the user|It can produce JSX to show content to the user
It can use *lifecycle method system* to run code at specific points in time.|**It can use *hooks* to run code at specific points in time.**
It can use the *state system* to update content on the screen.|**It can use *hooks* to access *state system* and update content on screen.**

Now both of them have same capabilities, which one should we learn?

Some companies with established projects are using class-based components. But some companies with newer projects may be using class-based or function-based components, so you still need to learn both.

Now you can refer to the [seasons](seasons/) project to learn following concepts. To set up this project, type the following commands:
```
cd seasons
yarn
yarn start
```

For this app, you can change your browser location to see different scenes.

### Geolocation API
We can use [Geolocation API](https://developer.mozilla.org/en-US/docs/Web/API/Geolocation_API) to get user's location.

```js
window.navigator.geolocation.getCurrentPosition(
  position => console.log(position),
  err => console.log(err)
);
```

Notice that when we call geolocation service, we need to wait some time to get result. But code running is keep going, it does not wait for this API. This function is called *async function*. When async function finishes, it will call a *callback function*.

There are 2 arguments we have to provide for this API, these 2 arguments are actually 2 functions. Yes, they are *callback functions*, one is called when we get success, another is called when get failure.

You can copy and paste above code on browser console, and allow access your location. After few second, you can get a `GeolocationPosition` in your console.
<br/><img src="./screenshots/location-allow.png" width="300"/><br/>

If you want to change your location, you can open browser sensors like so:
<br/><img src="./screenshots/sensors.png" width="500"/><br/>

Then choose the place you'd like. Then type code again, you can see your browser location is changed.
<br/><img src="./screenshots/san-francisco.png" width="400"/><br/>

### State

*State* is a JS object that contains data relevant to a component. There are some rules of state:
- Updating state on a component causes the component to (almost) instantly rerender.
- State must be initialized when a component is created.
- State can ***ONLY*** be updated using the function `setState()`.

There are 2 ways to initialize state:
- You can initialize state through `constructor()`.
    ```js
    class App extends React.Component {
      constructor(props) {
        super(props);
        this.state = { lat: null, errorMessage: '' };
      }
    }
    ```
- You can use `state` (instead of `this.state`) to initialize. Actually this way is same with first one, *Babel* will translate it to put state into constructor.
    ```js
    class App extends React.Component {
        state = { lat: null, errorMessage: '' };                
        ...
    }
    ```

If we want to update the state, we have to call `setState()`. Do not try to assign state directly except for initialization.
```js
class App extends React.Component {
  constructor(props) {
    super(props);

    // This this the ONLY time we do direct assignment to 'this.state'
    this.state = { lat: null, errorMessage: '' };

    window.navigator.geolocation.getCurrentPosition(
      position => {
        // We called setState()
        this.setState({ lat: position.coords.latitude });

        // We DO NOT!!
        this.state.let = position.coords.latitude

      },
      err => this.setState({ errorMessage: err.message })
    );
  }
```

You can pass state as props like so:
```js
if (!this.state.errorMessage && this.state.lat) {
  // Pass state 'lat' to 'SeasonDisplay'
  return <SeasonDisplay lat={this.state.lat} />;
}
```
```js
const SeasonDisplay = (props) => {
  // Take state from props
  const season = getSeason(props.lat, new Date().getMonth());
  ...
};
export default SeasonDisplay;
```

### Components Lifecycle
The component has a lot of events, the entire series of events is called components lifecycle. See following table:

Time Order|Component|Suitable for What
--|--|--
1|`constructor()`|Good place to do one-time setup, avoid to do data-loading here.
2|`render()`|Avoid doing anything besides returning JSX.
3|Content visible on screen|
4|`componentDidMount()`|Good place to do data-loading.
5|Wait for updates...|
...|...|...
6|Call `render()` to update|
7|`componentDidUpdate()`|Good place to do more data-loading when state/props change.
8|Wait until this component is no longer shown|
9|`componentWillUnmount()`|Good place to do cleanup (especially for non-React stuff).

There are 3 other lifecycle methods (rarely used): `shouldComponentUpdate`, `getDerivedStateFromProps`, and `getSnapshotBeforeUpdate`.

### Default Props
If you want to give the child component default value of props, there are 2 ways to do:
- Use `||` to set default value.
    ```js
    const Spinner = props => {
      return (
        <div className="ui active dimmer">
          <div className="ui big text loader">{props.message || 'Loading...'}</div>
        </div>
      );
    };
    ```
- Use `defaultProps` to set default value.
    ```js
    Spinner.defaultProps = {
      message: 'Loading...'
    };
    ```

### Tips for Render Method
`render()` method is going to get called all the time, so don't put any request or do any work in this method.

In general, we always try as much as possible to not have multiple `return` statement inside the `render()` method. You should use helper method like so:
```js
renderContent() {
  if(this.state.errorMessage && !this.state.lat) {
    return <div>Error: {this.state.errorMessage}</div>;
  }
  if(!this.state.errorMessage && this.state.lat) {
    return <SeasonDisplay lat={this.state.lat} />;
  }
  return <Spinner message="Please accept location request" />;
}

render() {
  return (
    <div>{this.renderContent}</div>
  )
}
```

## Event Handling
In this section, we will learn how to get feedback from the user and how to fetch data from some outside API or server.

You can refer to the [pics](pics/) project to learn following concepts. Follow the steps: 

1. Go to [Unsplash](https://unsplash.com/developers) and register as a developer. *Unsplash* is a company that focuses on stock images or essentially images of all types.
1. Create a new application and copy the **Access Key** in your application.
1. Go to [unsplash.js](pics/src/api/unsplash.js) and replace `<YOUR_ACCESS_KEY>` with your access key.
1. Type the following commands to run service:
    ```
    cd pics
    yarn
    yarn start
    ```

For this app, you can search pictures with keyword like so:
<br/><img src="./screenshots/pics.png" width="800"/><br/>

### Callback Function
When we pass a function as a prop, this function is a *callback function*. We ***DO NOT*** put a set of parentheses at the end of it.

If you put a set of parentheses right there that means it's automatically called whenever a component is rendered. But we don't want to call this function when our component is rendered. Instead we want to call this function at some point in time in the future.

```js
onInputChange(event) {
  console.log(event.target.value);
}

render() {
  return (
    ...
    <input type="text" onChange={this.onInputChange}/>
  );
}
```

There is another syntax to make a callback function same with above method called *arrow function*.
```js
render() {
  return (
    ...
    <input type="text" onChange={e => console.log(e.target.value)}/>
  );
}
```

The naming rule of callback function is `on + <NAME_OF_THE_ELEMENT> + <EVENT>`. For example, `onInputChange` and `onInputClick`.

### Uncontrolled Elements
When we store the value inside of HTML element directly instead of using React state, this element is called *uncontrolled elements*. If we want to know what value was right now, the only way is to reach into the DOM find that element and pull the value out of it.

In the very slim period of time, the React and HTML are not consistent. The above example is a *uncontrolled element*, the property `e.target.value` stores the user information directly. We don't set any value on it, so we don't control this value by React.


### Controlled Elements
The value is stored inside of React components on states property. HTML is rendered or driven by React, we can control the HTML element by React. That's why we prefer to use *control elements*. Notice that we override value by React state, because we make sure React driving and storing data to render the HTML.

The following code is controller version of above example. User types in input, then callback get invoked to update state. Component rerender (because `setState` is called), input is told what its value is.
```js
state = { term: ''};

render() {
  return (
    ...
    <input 
      type="text"
      value={this.state.term}
      onChange={e => this.setState({ term: e.target.value })}
    />
  );
}
```

### Keyword `this`
When `this` is used in a class, `this` is a reference to the instance of that class. For example, `this.state` and `this.render()`.

When `this` is used in a function, `this` is determined by who call this function. See the following example:
```js
class Car {
  setDriveSound(sound) {
    this.sound = sound;
  }
  drive() {
    return this.sound;
  }
}

const car = new Car();
car.setDriveSound('vroom');

const drive = car.drive;
const truck = {
  sound: 'pupupu',
  driveMyTruck: drive
}

// sound
car.drive();

// pupupu
truck.driveMyTruck();

// broken
drive(); 
```
There are 3 ways to fix issue:
- You can use `bind()` function in `constructor()` to bind `this` to `Car` instance.
    ```js
    class Car {
      constructor() {
        this.drive = this.drive.bind(this);
      }
      drive() {
        return this.sound;
      }
    }
    ```
- You can use *arrow function* to make sure `this` is always equal to the instance of class.
    ```js
    // The following functions will lead to a broken value of 'this'
    onFormSubmit(event) {
      event.preventDefault();
      console.log(this.state.term)
    }
    onFormSubmit: function(event) {
      event.preventDefault();
      console.log(this.state.term)
    }

    // Arrow function will make sure 'this' is always to the instance of class
    onFormSubmit = event => {
      event.preventDefault();
      console.log(this.state.term)
    }

    ```
- Calling *arrow function* in property can make sure `this` is equal to instance of the class.
    ```js
    onFormSubmit(event) {
      event.preventDefault();
      console.log(this.state.term)
    }

    render() {
      return (
        <form onSubmit={event => this.onFormSubmit(event)} className="ui form">...</form>
        ...
      );
    }
    ```

### Communicating Child to Parent
If child component wants to tell parent component some value, we can follow the steps:
1. Define the callback method in parent element ([App.js](pics/src/components/App.js)).
1. Pass that callback method as a prop down to the child element ([SearchBar.js](pics/src/components/SearchBar.js)).
1. Child element calls that callback to tell parent element. When making use of `props` inside of a class based component, we reference the `props` object with `this.props` (we can reference `props` in functional component directly).

### Axios
React app uses AJAX client to request the API. There are 2 ways to request API, *axios* (better) and `fetch()` function. *axios* is a third party package, `fetch()` is a built-in function in modern browsers.

When we want to call API using *axios*, there are 2 ways to get async data.
- Use `then()` syntax.
    ```js
    onSearchSubmit(term) {
      axios.get('https://api.unsplash.com/search/photos', {
        params: { query: term },
        headers: {
          Authorization: 'Client-ID <YOUR_ACCESS_KEY>'
        }
      }).then((response) => {
        console.log(response.data.results);
      });
    }
    ```
- Use `async` and `await` syntax.
    ```js
    async onSearchSubmit(term) {
      const response = await axios.get('https://api.unsplash.com/search/photos', {
        params: { query: term },
        headers: {
          Authorization: 'Client-ID <YOUR_ACCESS_KEY>'
        }
      });
      console.log(response.data.results);
    }
    ```

In general, we'd like to move configuration of `axios` to another class, e.g. [unsplash.js](./pics/src/api/unsplash.js).
```js
// unsplash.js
import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: 'Client-ID <YOUR_ACCESS_KEY>'
  }
});
```
```js
// App.js
import unsplash from '../api/unsplash';

class App extends React.Component {
  ...
  onSearchSubmit = async term => {
    const response = await unsplash.get('/search/photos', {
      params: { query: term },
    });
    
    this.setState({ images: response.data.results });
  }
  ...
}
```

### Rendering Lists
When we want to render a list like following code, we might use following code:
```js
const images = props.images.map((image) => {
  return <ImageCard image={image} />;
});
```
But we will get heads-up in console as follows:
```
Warning: Each child in a list should have a unique "key" prop.
```
Adding key for list is a purely performance consideration. When we use key, the React can compare key first instead of content, it helps React render list or update list to be more precisely and more performance.
```js
const ImageList = (props) => {
  const images = props.images.map((image) => {
    return <ImageCard key={image.id} image={image} />;
  });

  return <div className="image-list">{images}</div>;
};
```

### React Refs
*React Refs* system can give access to a single DOM element. We create refs in the `constructor()`, assign them to instance variables, then pass to a particular JSX element as props. For example, if we want to get the height of the image. We should render image first, then reach into the DOM to get this value.

Owing to creating refs in the `constructor()`, we will get this refs when this component is mount. Unfortunately, React needs some time to do something in DOM (e.g. download image). So in that moment, we'll get nothing but element tag in there.

Look at the following example, when we try to get value of `this.imageRef.current.clientHeight`, the download of image is still not finished yet, so we get `0`. But when we try to print out `this.imageRef`, we'll get `{current: img}`. When expanding it, we'll get a complete information, that's because the lazy fetching mechanism of the browser, when we expand it, the download is finished and browser fetches in that time.

```js
class ImageCard extends React.Component {
  constructor(props) {
    super(props);
    this.imageRef = React.createRef();
  }

  componentDidMount() {
    console.log(this.imageRef); // {current: img}
    console.log(this.imageRef.current.clientHeight); // 0
  }

  render() {...}
}
```

To fix this render issue, we can use a callback function `addEventListener()` to trigger the method when render is completely finished.
```js
class ImageCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = { spans: 0 };
    this.imageRef = React.createRef();
  }

  componentDidMount() {
    this.imageRef.current.addEventListener('load', this.setSpans);
  }

  setSpans = () => {
    const height = this.imageRef.current.clientHeight;
    const spans = Math.ceil(height / 10);
    this.setState({ spans });
  };

  render() {...}
}
```

## Videos App

This section is a practice for review the concept you learned. You can refer to the [videos](videos/) project, and follow the steps to set up:

1. Go to [Google APIs](http://console.developers.google.com) and register an account.
1. Generate a Google dev project.
    <br/><img src="./screenshots/google-project.png" width="1050"/>
1. Select **Library** on the left panel and search `youtube data api v3` then click on it.
1. Hit **Enable**.
    <br/><img src="./screenshots/youtube-data-api.png" width="350"/>
1. Select **Credentials** on the left panel, create a credential with API key.
    <br/><img src="./screenshots/create-credentials.png" width="600"/>
1. Restrict your key by adding `localhost:3000` in website restrictions.
    <br/><img src="./screenshots/key-restrictions.png" width="500"/>
1. Go to [youtube.js](videos/src/apis/youtube.js) and replace `<YOUR_YOUTUBE_API_KEY>` with your API key.
1. Type the following commands to run service:
    ```
    cd videos
    yarn
    yarn start
    ```

For this app, you can search vedios with keyword like so:
<br/><img src="./screenshots/videos.png" width="950"/><br/>
