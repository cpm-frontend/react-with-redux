# Context System

React version 16 introduce a revamped version of context system. Recall we learned props system before. Props system is to get data from a parent component to a **direct** child component. Context system is to get data from a parent component to **any** nested child component.

## Concepts

We are going to build a simple app to learn context system, see the mockup:
<br/><img src="./screenshots/translate-mockup.png" width="700"/>

You can refer to the [translate](translate/) project to learn following concepts. Follow the steps to set up:
1. Install packages.
    ```
    cd songs
    yarn
    ```
1. Type the following commands to run service:
    ```
    yarn start
    ```

### App Design
In this app, we need to pass language state to `Field` and `Button` components. Both components are under `UserCreate` component, but this component does not use this state. So this is a good change to use context system, because we don't need to pass state to `UserCreate` component.
<br><img src="./screenshots/context.png" width="500"/>

### Context Object
In context system, we use context object to communicate with components. Context object is like a pipe which has some data in it.

There are 2 ways to get information in this pipe, and there are also 2 ways to get information out of this pipe. See following figure:
<br/><img src="./screenshots/context-flow.png" width="500"/>

### Default Value
To use default value as source of data, all you need to do is to create a context object, see code in [LanguageContext.js](translate/src/contexts/LanguageContext.js):
```js
export default React.createContext("english");
```

To get this context object in `Button` component by using `this.context`, we have to set up a special variable (prop) called `contextType` (you can't use other name). It can connect to context object. There are 2 ways to set up this variable (prop) like so:
```js
// 1st way - inside of class
class Button extends Component {
  static contextType = LanguageContext;
}

// 2nd way - outside of class
class Button extends Component {...}
Button.contextType = LanguageContext;
```

After setting up `contextType`, you can use the context like a normal prop.
```js
class Button extends Component {
  static contextType = LanguageContext;
  console.log(this.context);    // english
  render() {...}
}
```

### Provider
But default value is just a default value, we cannot change it. If we want to change the data in context, we need to use `Provider` component. Notice that this `Provider` is different with `Provider` in Redux.

We want to get data from `App` component, so we import `LanguageContext` and use `LanguageContext.Provider` to wrap the component (i.e. `UserCreate`) we want to pass information. Notice that the `value` in `LanguageContext.Provider` is a special property for passing the information.
```js
class App extends Component {
  state = { language: 'english' };
  render() {
    return (
      <div>
        <LanguageContext.Provider value={this.state.language}>
          <UserCreate />
        </LanguageContext.Provider>
      </div>
    );
  }
}
```

> ### *Note*
> Each separate use of `LanguageContext.Provider` creates a new separate **pipe** of information. So if you use second `LanguageContext.Provider`, the context of them is totally independent.

### Consumer
You can also use `Consumer` component to consume the data in context. Notice that you need to pass a **function** as a child to `Consumer` component. This function will be called by `Consumer` automatically. The `value` is data in the context. You don't need to set up `contextType` in `Consumer` way. See the code in [Button.js](translate/src/components/Button.js):
```js
render() {
  return (
    <button className="ui button primary">
      <LanguageContext.Consumer>
        {value => (value === "english" ? "Submit" : "Voorleggen")}
      </LanguageContext.Consumer>
    </button>
  );
}
```
So what's different with using `Consumer` and `this.context`? When we use `this.context`, we can only access single context. If we want to access multiple context in a component, we need to use `Consumer`.

For example, we create second context called [ColorContext.js](translate/src/contexts/ColorContext.js) to handle color. In `App` component, we wrap `UserCreate` component by 2 `Provider` , the order doesn't matter.
```js
render() {
  return (
    <div>
      <LanguageStore>
        <ColorContext.Provider value="red">
          <UserCreate />
        </ColorContext.Provider>
      </LanguageStore>
    </div>
  );
}
```

See how we use both contexts in [Button.js](translate/src/components/Button.js).
```js
render() {
  return (
    <ColorContext.Consumer>
      {(color) => (
        <button className={`ui button ${color}`}>
          <LanguageContext.Consumer>
            {value => (value === "english" ? "Submit" : "Voorleggen")}
          </LanguageContext.Consumer>
        </button>
      )}
    </ColorContext.Consumer>
  );
}
```

### Redux vs Context
Redux and context system are kind of little different. Both of them can **distribute data to various components**, but Redux has 2 more features:
- Redux centralizes data in a store.
- Redux provides mechanism for changing data in the store.

**We don't recommend to replace Redux with Context system.** But if we want to use context system in place of Redux, there are some issues we need to think about:
- We need to be able to get data to any component in our hierarchy. Context system has ability to do this, so this is not an issue.
- It's easy to separate view logic from business logic in Redux. But in context system, we mix them.
- We need to be able to split up business logic (not have a single file with 10000 lines of code). It's easy in Redux, we can use action creators and reducers to split up the logic. But in context, we need to some works.

For last 2 issues, we have to refactor the code to fix.

### Refactoring
So we create a new component called `LanguageStore` to store all business logic. It also implement a Provider so that it can share that information with different child components.
<br/><img src="./screenshots/context-refactor.png" width="400"/><br/>

We put the logic in `LanguageStore` class, see the code in [LanguageContext](translate/src/components/LanguageContext.js):
```js
// You need to use capital C
const Context = React.createContext('dutch');

export class LanguageStore extends Component {
  state = { language: 'english' };

  onLanguageChange = (language) => {
    this.setState({ language });
  };

  render() {
    return (
      <Context.Provider
        value={{ ...this.state, onLanguageChange: this.onLanguageChange }}
      >
        {this.props.children}
      </Context.Provider>
    );
  }
}

export default Context;
```

Notice that we need to use capital C on the variable context. If we use a lowercase, React will think that is a normal vanilla HTML element that we are trying to render out.

The only benefit to use context system is no need for an extra library.
