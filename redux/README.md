# Redux

*Redux* is a *state* (same in React) management library. Notice that Redux is not required to create a React app and not explicitly designed to work with React.

Let's try to figure out Redux from an analogy.

## Analogy
Imagine we want to build an insurance company. There are 2 insurance terms here:
- Policy: Customer holds a *policy*, if bad stuff happens to them then we pay them.
- Claim: Customer had something bad happen to them, we need to pay them.

The form contains 2 parts. One is type (the purpose of this form), another is payload (what is this form trying to do). For example, there is a form like so:
```
Type: Create Claim
Payload:
  Name: Alex
  Cliam Amount: $500
```

So if someone wants to get a new policy, he/she will submit a form to form receiver. Receiver copies this form and passes it to related departments. The following figure shows up the process:
<br/><img src="./screenshots/insurance-co.png" width="700"/><br/>

Now imagine we have a management team wants to get some data from policies department. It's not ideal to store the data in a specific department, it's not efficiency and it could be inconsistence. So we create a central library to store all department data. If this management team wants to get data, it can visit this library directly.
<br/><img src="./screenshots/redux-analogy.png" width="600"/><br/>

The key point is how to update the data, see above figure. For example, when receiver passes the form to policies department, this department should take whole history data from central library along with this form. Now this department have complete data, it can do some calculation. Finally, it writes this new data back to central library.

As we mentioned, data will be passed to all departments. Each department only cares about certain forms with corresponding type. For example, if a form with a type 'Create Claim', claim history department will take some actions and update the data. But policies department doesn't care about this type of form, so when it receives this form, it will do nothing and write original data back to library.

Now we can map the analogy to Redux.
<br/><img src="./screenshots/redux-cycle.png" width="550"/><br/>

You can check the code in [codepen](./codepen.js), this code implements the above story. You can copy and paste the code on [Codepen](https://codepen.io/sgrider/pen/yRWZEq?editors=0010), remember to add `redux` in **Add External Scripts/Pens** of JavaScript in **Setting**.
<br/><img src="./screenshots/codepen-setting.png" width="650" /><br/>

## React with Redux

In this section, we are going to learn how to use Redux in React app. Let's see the mockup:
<br/><img src="./screenshots/songs-mockup.png" width="400"/><br/>

In this app, we can click on **Select** button to select a song. The details of this song will show up. You can refer to the [songs](songs/) project to learn following concepts. Follow the steps to set up:
1. Install packages.
    ```
    cd songs
    yarn
    ```
1. Type the following commands to run service:
    ```
    yarn start
    ```

### App Design
Let's think how to build this app by React first. The design might be like so:
<br/><img src="./screenshots/without-redux.png" width="350"/>

We need a callback function `onSongSelect()`, when user clicks button to choose the song, it could tell `App` to update the selected song.

Let's take a look for design by using Redux:
<br/><img src="./screenshots/with-redux.png" width="500"/>

There are 2 new components `Provider` and `connect` that are implemented by *React Redux*.

Redux store contains all of reducers and all the states of the app. We pass this store as props to `Provider`.

`Provider` is providing information to all of the different components inside of app. When a component (e.g. `SongList`) wants to use state in `Provider`, it can get via `connect` by *context system* that can ignore any component between parent component and child component and communicate with each other.

`connect` also handles the action which action creator sends to. So we will use `connect` like so (`selectSong` is an action):
```js
export default connect(mapStateToProps, { selectSong })(SongList);
```

The common Redux structure is like so:
```
/src
  /actions    : Contains files related to action creators
  /components : Files related to components
  /reducers   : Files related to reducers
  index.js    : Sets up both the react and redux side of the app
```

Add `index.js` for the folder, then you can use shortcut in importing. For example, we add `index.js` in actions folder, then we can easily import it by `../actions` instead of `../actions/index` in `App.js`.
```js
// App.js
import actions from '../actions';
```

### Named Export vs Default Export
When we use *named export*, we need curly braces.
```js
// actions/index.js
export const selectSong = song => {...};
```
```js
// App.js
import { selectSong } from "../actions";
```

When we use *default export*, we don't need curly braces.
```js
// actions/index.js
const selectSong = song => {...};
export default selectSong;
```
```js
// App.js
import selectSong from "../actions";
```

### Reducers
We need 2 reducers to handle song list and selected song. Check the code in [reducers/index.js](./songs/src/reducers/index.js):
```js
const songsReducer = () => {
  return [
    { title: 'No Scrubs', duration: '4:05' },
    { title: 'Macarena', duration: '2:30' },
    { title: 'All Star', duration: '3:15' },
    { title: 'I Want it That Way', duration: '1:45' },
  ];
};

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === 'SONG_SELECTED') {
    return action.payload;
  }
  return selectedSong;
};

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer,
});
```

For `songsReducer`, it's totally static data, we don't need to use reducer. We use it just for demo and not necessary.

For `selectedSongReducer`, we can deal with different actions. `selectedSong` is state, we store this state again after `selectedSongReducer` is executed.

Finally, we need to combine reducers by using `combineReducers()` to export them.

### Component `Provider`
We use `Provider` to take care our store. Notice that `Provider` will be rendered before app component. Check the code in [src/index.js](./songs/src/index.js): 
```js
ReactDOM.render(
  <Provider store={createStore(reducers)}>
    <App />
  </Provider>,
  document.querySelector('#root')
);
```

### Component `connect`
Every time the reducers change in store, the `Provider` will notify the `connect` (it's a push model for this subscriber `connect`) and `connect` is going to pass the state in reducers down to the component.

We need a method called `mapStateToProps` to take the `Provider`'s state and do some calculation, finally map it to props of this component. Notice that the naming of `mapStateToProps` is convention, you can name it any word, but we should follow naming rule here.

After component connected, now we can use state as props in this component. For example, see the code in [SongList](songs/src/components/SongList.js). We want to use `song` state in reducer, so we use `mapStateToProps` to map this state to `song` prop. Then we can use this prop in this component directly.
```js
const mapStateToProps = state => {
  return { songs: state.songs };
};

// Connect SongList with store (all reducers)
export default connect(mapStateToProps)(SongList);
```

`connect` can connect the component with reducers, it's also used to get action creators correctly into the components.
```js
// Connect SongList with store and action creator 'selectSong' 
export default connect(mapStateToProps, { selectSong })(SongList);
```

> ### *Note*
> **Why we pass action creator to `connect` as opposed to just directly calling it inside of the component?**
>
> If we just call it directly, that's just a function returning a action, and this function isn't forwarded to Redux. If we want to forward the function (action) to Redux, we need to use `dispatch()`. `connect` can wrap the function passed as parameters up and automatically call `dispatch()` for us.

### Redux Process
Let's see what happens when user clicks on **Select** button:
1. User clicks the button **Select**.
1. Call an action creator `selectSong`.
1. Action `selectSong` is wrapped by `connect` and dispatched to reducer in store.
1. Reducer `selectedSongReducer` receives this action and do some logic to update the state.
1. After state is updated, reducer notifies subscriber `SongList` (which uses `connect` and `mapStateToProps` to listen reducers).
1. Props of `SongList` of the component is updated.
1. Page is rerendered.

## Redux Thunk

In this section, we are going to build an easy blog app just for introducing *Redux Thunk*. This app shows up some content, the data is from [JSON Placeholder API](https://jsonplaceholder.typicode.com/). See mockup:
<br/><img src="./screenshots/blog-mockup.png" width="400"/><br/>

You can refer to the [blog](blog/) project to learn following concepts (this section and [Redux Store Design](./README.md#redux-store-design) section). Follow the steps to set up:
1. Install packages.
    ```
    cd songs
    yarn
    ```
1. Type the following commands to run service:
    ```
    yarn start
    ```

### Fetching Data
In general, we fetch data with Redux as following figure:
<br/><img src="./screenshots/general-data-loading.png" width="550"/><br/>

In this case, we want to get data from external API as soon as component did mount. So we call this API in action creator like so:
```js
// Action creator
export const fetchPosts = async () => {
  const response = await jsonPlaceholder.get("/posts");

  return {
    type: "FETCH_POSTS",
    payload: response
  };
};
```

In fact, above code is bad approach. Because we are breaking the rules of Redux (we will explain later). When you open your browser, we will get error message:
```
Error: Actions must be plain objects. Use custom middleware for async actions.
```

So what's wrong with `fetchPosts` method?
- Action creators ***MUST*** return plain JS objects with a type property. When we are using `await` and `async`, we actually return a request object instead of plain object.
- By the time our action gets to a reducer, we don't have fetched the data. If you remove `await` and `async`, you will no longer get errors, but the result is not going to work as you expect. Because you need some time to get response, without the `async` syntax you do not get some data back.

Now we know there are 2 types of action creators:
- Synchronous (sync) action creator: Instantly returns an action with data ready to go.
- Asynchronous (async) action creator: Takes some amount of time for it to get its data ready to go. If async action creator in the application, you have to install a *middleware*.

### Middleware

Middleware is a plain JS function that gets called with every action we dispatch. It has the ability to stop, modify or otherwise mess around with actions. There are many open source middleware you can use. Most popular use of middleware is for dealing with async action creators. We will use a middleware called *Redux Thunk* to solve the async issues.

Unlike normal Redux, when we use Redux Thunk, Redux won't dispatch action to reducer directly. Instead, it will dispatch action to middleware like following figure:
<br/><img src="./screenshots/redux-cycle-with-middleware.png" width="450"/><br/>

Let's compare action creator rules:
Normal Rules|Rules with Redux Thunk
--|--
Action creator must return action objects.|Action creators can return action objects **or functions**.
Actions must have a type property.|If an action object gets returned, it must have a type.
Actions can optionally have a payload.|If an action object gets returned, it can optionally have a payload.

The following figure shows up how Redux Thunk works:
<br/><img src="./screenshots/redux-thunk.png" width="400" /><br/>

If this action is an object, Redux Thunk will pass it to reducers as usual.

If this action is a funcion, Redux Thunk will invoke this function with `dispatch()` and `getState()` functions as arguments like so:
```js
export const fetchPosts = () => {
  return function(dispatch, getState) {...}
};
```

`dispatch()` can change any data and `getState()` can read any data you want. Next, we'll wait for the request to finish. After request is complete, Redux Thunk will dispatch action **manually**. In other word, we can manually dispatch an action in the future. After dispatching action, you will get a new action, and it will dispatch to Redux Thunk to see whether is a function or a plain object.

You can take a look for Redux Thunk [source code](https://github.com/reduxjs/redux-thunk/blob/master/src/index.js). It's only 14 lines!
```js
function createThunkMiddleware(extraArgument) {
  return ({ dispatch, getState }) => next => action => {
    if (typeof action === "function") {
      return action(dispatch, getState, extraArgument);
    }

    return next(action);
  };
}

const thunk = createThunkMiddleware();
thunk.withExtraArgument = createThunkMiddleware;

export default thunk;
```

To use Redux Thunk, see the code in [src/index.js](blog/src/index.js):
```js
const store = createStore(reducers, applyMiddleware(thunk));

ReactDOM.render(
  <Provider store={store}><App /></Provider>,
  document.querySelector('#root')
)
```

See the code in [actions/index.js](blog/actions/index.js), you can return or not return in the inner function. What we are concern about is returning outer function, the actual creator itself.
```js
export const fetchPosts = () => {
  return async function(dispatch, getState) {
    const response = await jsonPlaceholder.get("/posts");
    dispatch({ type: "FETCH_POSTS", payload: response });
  };
};

// After refactoring as follows
export const fetchPosts = () => async dispatch => {
  const response = await jsonPlaceholder.get("/posts");
  dispatch({ type: "FETCH_POSTS", payload: response });
};
```

## Redux Store Design
When we use or design a reducer, there are some rules we have to follow:
1. ***MUST*** return any value besides `undefined`.
    ```js
    // BAD!
    export default () => {};
    export default () => undefined;

    // GOOD!
    export default () => "string";
    export default () => null;
    export default () => [];
    ```
1. We produce **state** or data to be used inside of app using only previous state and the action. So for the first time, we need a default value. Because previous state is `undefined`.
    ```js
    const selectedSongReducer = (selectedSong = null, action) => {...};
    ```
1. ***MUST NOT*** return reach **out of itself** to decide what value to return (reducers are pure).
    ```js
    export default (state, action) => {
      // BAD!
      return document.querySelector("input");
      return axios.get("/posts");

      // GOOD!
      return state + action;
    };
    ```
1. Must not mutate its input state argument (rule for beginners).
    ```js
    export default (state, action) => {
      // BAD! (mutate array)
      state[0] = "Sam";
      state.pop();
      state.push("Tom");

      // BAD! (mutate object)
      state.name = "Same";
      state.age = 30;
    };
    ```

The last rule is only for beginners, actually, you can mutate it all day and not see any errors. Because it's easier to tell beginners **don't mutate state ever** than to tell them **when they can and can't**.

If you just mutate state, the state is still itself. So reducer will not notify system this change (because reference of this state is same).
```js
export default (state, action) => {
  state.name = "Same";
  state.age = 30;
  // You can do this, but the reducer will not notify system this change, because state === state
  return state;
};
```
### Safe State Updates
Here is a note for safe state updates in reducers:

Operations|Bad|Good
--|--|--
Removing an element from an array|`state.pop()`|`state.filter(element => element !== 'hi')`
Adding an element to an array|`state.push('hi')`|`[...state, 'hi']`
Replacing an element in an array|`state[0] = 'bye'`|`state.map(el => el === 'hi' ? 'bye' : el)`
Updating a field in an object|`state.name = 'Sam'`|`{...state, name: 'Sam'}`
Adding a field to an object|`state.age = 30`|`{...state, age: 30}`
Removing a field from an object|`delete state.name`|`{...state, age: undefined}` (not so good)<br/>`_.omit(state, 'age')` (lodash required)

We usually use switch statement to handle different cases, see [usersReducers.js](blog/src/reducers/usersReducer.js).
```js
export default (state = [], action) => {
  switch (action.type) {
    case 'FETCH_USER':
      return [...state, action.payload];
    default:
      return state;
  }
};
```

### Reducing API Calls
When we call API to fetch posts, there are 100 items in returned array, each user has 10 posts. The problem here is if we go through all posts after fetching posts, then we call API by user ID to fetch detail 100 times, it's not ideal. Because only 10 users, we should avoid this duplication.

There are 2 ways to reduce calling times in action.
- We can use `memoize()` function provided by *Lodash*, see details [here](./README.md#function-memoize). Notice that this solution cannot handle the refetch case. So if we need to refetch user, we have to create another function without `memoize()`.
    ```js
    export const fetchUser = (id) => (dispatch) => _fetchUser(id, dispatch);
    const _fetchUser = _.memoize(async (id, dispatch) => {
      const response = await jsonPlaceholder.get(`/users/${id}`);
      dispatch({ type: 'FETCH_USER', payload: response.data });
    });
    ```
- We can create a new function to handle fetching posts and users case. See the following code or code in [actions/index.js](blog/src/actions/index.js) (after refactoring). Notice that we should not use `fetchPosts()` and `fetchUser()` without `dispatch()`, because we need to let this function go through Redux. When getting list of posts, we need to use `await` to make sure we get the response, but if we don't need to use the response like fetching user in last line, we don't need to use `await`.
    ```js
    export const fetchPostsAndUsers = () => async (dispatch, getState) => {
      // Call 'fetchPosts' and get list of posts
      await dispatch(fetchPosts());

      // Find all unique userId's from list of posts
      const userIds = _.uniq(_.map(getState().posts, 'userId'));

      // Iterate over unique userId and call 'fetchUser' with each userId
      userIds.forEach((id) => dispatch(fetchUser(id)));
    };
    ```

> ### Function `memoize()`
> Be careful to use this function. If you memorize the outer function, that would be wrong. Because inner function is called by Redux Thunk. We should focus on inner function.
> ```js
> export const fetchUser = _.memoize(function(id) {
>   return async function(dispatch) {
>     const response = await jsonPlaceholder.get(`/users/${id}`);
>     dispatch({ type: "FETCH_USER", payload: response.data });
>   };
> });
> ```
> This following code is still bad. Because when we call `fetchUser`, we will recreate inner function (async function here) every time and memorize it. Of course, it's not going to work.
> ```js
> export const fetchUser = function(id) {
>   return _.memoize(async function(dispatch) {
>     const response = await jsonPlaceholder.get(`/users/${id}`);
>     dispatch({ type: "FETCH_USER", payload: response.data });
>   });
> };
> ```
> The following code is correct version. When we call `fetchUser`, we should get a memorized function instead of recreating function every time.
> ```js
> export const fetchUser = function (id) {
>   return function (dispatch) {
>     _fetchUser(id, dispatch);
>   };
> };
> const _fetchUser = _.memoize(async (id, dispatch) => {
>   const response = await jsonPlaceholder.get(`/users/${id}`);
>   dispatch({ type: 'FETCH_USER', payload: response.data });
> });
> ```

## React Router
We are going to build a streaming app like [Twitch](https://www.twitch.tv/directory/game/programming). In *Twitch*, every user just has one stream/channel they can stream to. But in our app, every user can create unlimited channels/streams that they can stream to. See the design:
<br/><img src="screenshots/streams.png" width="650" /><br/>

Here are some features we will build in this app:
||User is ***NOT*** Logged in|User is Logged in|
--|--|--
View a list of all streams/channels|V|V
View a video for a single stream/channel|V|V
Create a new stream/channel||V
Edit a stream/channel they've created||V
Delete a stream/channel they've created||V

You can refer to the [streams](streams/) project to learn following concepts ([React Router](./README.md#react-router), [Authentication](./README.md#authentication), [Redux Form](./README.md#redux-form), [REST](./README.md#rest), [React Portals](./README.md#react-portals), and [Streaming](./README.md#streaming) section). 

We are going to use Google OAuth for login. If you don't want to use my client ID, you can refer to [Set up Google OAuth](./README.md#set-up-google-oauth) section to generate your OAuth client ID and replace the variable `CLIENT_ID` in [GoogleAuth.js](streams/client/src/components/GoogleAuth.js).

Follow the steps to set up:

1. Install packages and start API server.
    ```
    cd streams/api
    yarn
    yarn start
    ```
1. Create second terminal to install packages and start client.
    ```
    cd streams/client
    yarn
    yarn start
    ``` 
1. Create third terminal to install packages and start RTMP server.
    ```
    cd streams/rtmpserver
    yarn
    yarn start
    ```
1. Install and set up OBS, follow the steps in OBS section [here](./README.md#obs).
1. Go to OBS, click **Settings** and set up the **Stream** as follows. **Stream Key** is your stream ID, see more details [here](https://github.com/illuspas/Node-Media-Server#from-obs).
    <br/><img src="./screenshots/stream-setting.png" width="950"/>
1. Click **Start Streaming** in OBS.
1. Go to your service and choose the corresponding stream ID, and click player icon in video you'll see the streaming.

### Paths
We will use *React Router* to handle the navigation. There are some packages in *React Router* family:
- `react-router`: It's a core navigation library, we don't install this manually.
- `react-router-dom`: It's a navigation for DOM-based apps. We need this.
- `react-router-native`: It's a navigation for React-native app.
- `react-router-redux`: It binds between Redux and *React Router*.

See the following code:
```js
import { BrowserRouter, Route } from "react-router-dom";

const PageOne = () => {...};
const PageTwo = () => {...};

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <div>
          <Route path="/" exact component={PageOne} />
          <Route path="/pagetwo" component={PageTwo} />
        </div>
      </BrowserRouter>
    </div>
  );
};
```
The rule of React Route handling paths is to extract the URL after domain name and port, and match the `path` attribute of all `Route`. 

`BrowserRouter` component will internally create an object `history` to keep track of address bar in the browser. `BrowserRouter` will communicate the path down to `Route` component.
<br/><img src="screenshots/react-router.png" width="500"/><br/>

If you remove `exact` in above example like following code:
```js
<Route path="/" component={PageOne} />
<Route path="/pagetwo" component={PageTwo} />
```

When you visit http://localhost:3000/pagetwo, `PageOne` and `PageTwo` components are shown in this page. That's because `/pagetwo` is matched to `/` and `/pagetwo`, so the *React Router* will show 2 components.

If you want to avoid to show `PageOne` component when visiting http://localhost:3000/pagetwo, you need to add `exact` or `exact={true}` like following code, that means the extracted path needs to be **equal** to `path`, then the component will be rendered (so `PageOne` will not be rendered).
```js
<Route path="/" exact component={PageOne} />
<Route path="/pagetwo" component={PageTwo} />
```

You can use same path in different `Route`, see the following code:
```js
<Route path="/" exact component={PageOne} />
<Route path="/" component={PageOne} />
<Route path="/" component={PageOne} />
<Route path="/pagetwo" exact component={PageTwo} />
```

We can use `:abc` to represent the variable `abc`. To use it, we can replace `:abc` with any word we want. e.g., path `/pages/:id` can be matched to the path `/pages/3`, and the `id` is 3.

Consider the following code, if you visit `/pages/new`, you'll find the 2 component are rendered, because `/pages/new` matches 2 rules.
```js
<Route path="/pages/new" exact component={PageOne} />
<Route path="/pages/:id" exect component={PageTwo} />
```

We can use `<Switch>` to avoid above problem. `<Switch>` lets the address just fit one path.
```js
<Switch>
  <Route path="/pages/new" exact component={PageOne} />
  <Route path="/pages/:id" exect component={PageTwo} />
</Switch>
```

### Navigation
We can use `<a />` to navigate but it's ***BAD***. When you use `<a />` and click it, your browser makes a request to path you defined. The server will response with a `index.html` file. The browser receives this `index.html` file, dumps old HTML file it was showing, so all your React and Redux state data are gone. It's not what we do in single page app (SPA).
```js
const PageOne = () => {
  // BAD!!!
  return (
    <div>PageOne<a href="/pagetwo">to 2</a></div>
  );
};
```

The correct method is to use `<Link />` like as follows. 
```js
const PageOne = () => {
  return (
    <div>PageOne<Link to="/pagetwo">to 2</Link></div>
  );
};
```
When user clicks a `<Link />`, *React Router* prevents the browser from navigating to the new page and fetching new `index.html`. Then, URL is going to change, `History` sees updated URL, takes URL and sends it to `BrowserRouter`. `BrowserRouter` communicates the URL to Route components, finally the Route components rerendered to show new set of components.

### Router Types
Besides `BrowserRouter`, there are 2 types of router you can use.

`HashRouter` uses everything after a `#` as the `path`. If you type http://localhost:3000/pagetwo, it will transfer to http://localhost:3000/#/pagetwo directly.
```js
<HashRouter>
    <div>
        <Route path="/" exact component={PageOne} />
        <Route path="/pagetwo" exact component={PageTwo} />
    </div>
</HashRouter>
```
`MemoryRouter` doesn't use the URL to track navigation. So the URL on your browser is always like http://localhost:3000/.
```js
<MemoryRouter>
    <div>
        <Route path="/" exact component={PageOne} />
        <Route path="/pagetwo" exact component={PageTwo} />
    </div>
</MemoryRouter>
```        

So Why do we need the routers above? In traditional HTML generating server, the client is looking for content for the route `/pagetwo`, if server has a route for `/pagetwo`, it will generate HTML and send it back. If not, it will response with 404.

In create-react-app dev server, when the server receive this route request, it checks dev resources first, and then check `public/` directory. If nothing special for route `/pagetwo`, it will serve up the `index.html` file. So the key point is here, when `index.html` is back, **the client will render it to the route `/pagetwo` by itself**.

In development environment, the create-react-app does this for us, but in production we have to set up by ourself, it's not easy and different from development. So we can use `HashRouter`, it will add `/#` automatically, the purpose is to tell server ignore the path after `/#`, that's for client. So the server will response the `index.html` like development. Same with `MemoryRouter`, it always request `/`, and memory the path for client side.

### Header
You can always show something (e.g. header) in any pages like following code:
```js
const App = () => {
  return (
    <div>
      <h1>Always Visible Header</h1>
      <BrowserRouter>...</BrowserRouter>
    </div>
  );
}
```
But if you want to use `<Link />` in your header, you'll get error say: **You should not use `<Link>` outside a `<Router>`**. So you need to put it into `<BrowserRouter>`, see [App.js](streams/client/src/components/App.js).
```js
const App = () => {
  return (
    <div>
      <BrowserRouter>
        <div>
          <Link>...</Link>
          <Switch>...</Switch>
        </div>
      </BrowserRouter>
    </div>
  );
}
```

## Authentication
We want user to login this app with Google. See the following mockup:
<br/><img src="./screenshots/oauth-mockup.png" width="700"/><br/>

Some information or actions (e.g. email and profile in above figure) user is asked from app, it's so-called **scopes**. You can find out Google Oauth scopes API [here](https://developers.google.com/identity/protocols/googlescopes).

Here are some difference between traditional authentication and OAuth authentication.
<br/><img src="./screenshots/oauth.png" width="550"/><br/>

Let's compare OAuth for servers and for JS browser apps:

OAuth for Servers|OAuth for JS Browser Apps
--|--
Results in a *token* that a browser app can use to make requests on behalf of the user|Same
Usually used when we have an app that needs to access user data while they are **not logged in**|Usually used when we have an app that only needs to access user data while they are **logged in**
Difficult to setup because we need to store a lot of info about the user|Very easy to set up thanks to Google's JS lib to automate flow

### Set up Google OAuth
Google OAuth flow is as follows:
<br/><img src="./screenshots/oauth-flow.png" width="500" /><br/>

To set up Google OAuth, follow the steps:
1. Go to [Google APIs](http://console.developers.google.com) and generate a Google dev project.
1. Select **OAuth consent screen** in left panel and choose **External** and click **CREATE**.
1. Type `streamy` for your app name, and type your email for **User support email** and **Developer contact information**. Then, click **SAVE AND CONTINUE**.
1. In **Scopes** step, click **ADD OR REMOVE SCOPES** and choose **.../auth/userinfo.email**. Then click **UPDATE** button to update and click **SAVE AND CONTINUE** to next step.
    <br/><img src="./screenshots/oauth-scope.png" width="600"/>
1. For **Test users** step, click **SAVE AND CONTINUE** to next step.
1. Select **Credentials** in left panel and click **CREATE CREDENTIALS** with **OAuth client ID**.
1. Input data for generating OAuth client ID like so and click **CREATE**.
    <br/><img src="./screenshots/oauth-id.png" width="550"/>
1. You should see a pop-up which shows up your client ID and client secret. We are going to use client ID for our case, client secret is used in case of OAuth for server. Copy your client ID in your editor, we will use it later.
    <br/><img src="./screenshots/client-id.png" width="450"/>

NPM doesn't provide Google API library, so we have to use script tag to install it manually. To use this client ID, follow the steps:

1. Go to [index.html](streams/client/public/index.html) and add a script like so:
    ```html
    <head>
      <script src="https://apis.google.com/js/api.js"></script>
    </head>
    ```
1. Go to console in your browser and type `gapi` you'll see it has only `load` method. You need to load library you want via this method. So type following command in your console to download the OAuth client library.
    ```js
    gapi.load('client:auth2')
    ```
1. Now you can test it in your browser console by tying `gapi` command, you should see something like so:
    <br/><img src="./screenshots/gapi.png" width="250"/>
1. In our app, we need to add the above method in [GoogleAuth.js](streams/client/src/components/GoogleAuth.js). Replace `<YOUR_CLIENT_ID>` with your client ID in the following code:
    ```js
    componentDidMount() {
      window.gapi.load("client:auth2", () => {
        window.gapi.client.init({
          clientId: "<YOUR_CLIENT_ID>",
          scope: "email"
        });
      });
    }
    ```

You can see more content in [Google Sign-In for Websites](https://developers.google.com/identity/sign-in/web/reference). Here are some basic commands:
- You can get a reference to the `auth` object after it's initialized. Try following code in your console:
    ```js
    const auth = gapi.auth2.getAuthInstance()
    auth.signIn()
    auth.signOut()
    ```
- Figure out if the user is currently signed in.
    ```js
    auth.isSignedIn.get()
    ```
- To listen the current status and update automatically without refresh. See [GoogleAuth.js](streams/client/src/components/GoogleAuth.js) to get complete usage.
    ```js
    auth.isSignedIn.listen(...)
    ```

### OAuth in Redux Design
This app we'll use the following structure, but it's not Redux convention because this example is very easy.
<br/><img src="./screenshots/oauth-redux-1.png" width="700"/><br/>

The following screenshot is Redux design convention, you'll see we call Google API in action creator.
<br/><img src="./screenshots/oauth-redux-2.png" width="700" /><br/>

### Redux DevTools
[Redux DevTools](https://github.com/zalmoxisus/redux-devtools-extension) is a great tool to debug when you develop with Redux project. If you are using Chrome, download [here](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd).

If you setup your store with middleware and enhancers, change (see more details [here](https://extension.remotedev.io/#12-advanced-store-setup)):
```js
// src/index.js
import { createStore, applyMiddleware, compose } from 'redux';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, composeEnhancers(applyMiddleware()));
```

Open Redux DevTools in your browser, you can see the following figure: 
<br/><img src="./screenshots/redux-devtool.png" width="600" />

For debug session, you can use `http://localhost:3000?debug_session=<STR>` to store all data in Redux store between refreshes of the page. You can replace `<STR>` with any string. Furthermore, you can use different session to store different scenarios. e.g. http://localhost:3000?debug_session=logged_in and http://localhost:3000?debug_session=logged_out.

## Redux Form
*Redux Form* is a library to help you handle all those different forms and integrate all that stuff with Redux. Without Redux, we are used to handle form like so:
<br/><img src="./screenshots/without-redux-form.png" width="500"/><br/>

With Redux Form, you don't need to create a reducer for forms by yourself.
<br/><img src="./screenshots/with-redux-form.png" width="600"/><br/>

You can take a look for some great examples from official [website](https://redux-form.com/7.4.2/examples/).

To use Redux Form in our app, we need to import reducer first. As mentioned, Redux Form created a reducer for us, all we need to do is to import it. If you feel confused with the naming, you can rename it like `formReducer`. See the code in [reducer/index.js](streams/client/src/reducers/index.js).
```js
import { reducer as formReducer } from "redux-form";

export default combineReducers({
  form: formReducer
});
```

If we want to use this reducer (i.e. `formReducer`) in a specific component, we can use `reduxForm` component to connect. This component is same with the `connect` component in `react-redux`. It makes sure that we can call some action creator and get some form data into the component. See the code in [StreamForm.js](streams/client/src/components/streams/StreamForm.js).
```js
export default reduxForm({
  // The name of this form will be whatever the purpose of the form is
  form: 'streamForm'
})(StreamForm);
```

`Field` is a component in Redux Form. We make use of this component anytime to show a field to the user, a field is some type of input. It can be a checkbox or a radio button and so on. `name` argument is required, it will be passed into the component. We can add any props we want, if `Field` doesn't recognize it, `Field` will take that prop and pass it through the component, see complete code in [StreamForm.js](streams/client/src/components/streams/StreamForm.js).
```js
renderInput = ({ input, label }) => {
  return (
    <div>
      <label>{label}</label>
      <input
        onChange={input.onChange}
        value={input.value}
      />
    </div>
  );
};

// Redux Form call 'event.preventDefault()' for us automatically
onSubmit = (formValues) => {
  this.props.onSubmit(formValues);
};

render() {
  return(
    <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
      <Field name="title" component={this.renderInput} label="Enter Title"/>
    </form>
  )
}
```

In above code, you can notice that Redux Form provides `handleSubmit()` function for us, it will pass all the values in the form (i.e. `formValues`) instead of `event` object to `onSubmit()` function. Because user can pass empty string as value to `onSubmit()` function, so we need a validator to check.

### Validation
We can create a function as follows and put it as argument into `reduxForm` compoennt.
```js
const validate = (formValues) => {
  const errors = {};
  if (!formValues.title) {
    errors.title = 'You must enter a title';
  }
  if (!formValues.description) {
    errors.description = 'You must enter a description';
  }
  return errors;
};

export default reduxForm({
  form: 'streamForm',
  validate,
})(StreamForm);
```

When the form is initially rendered or user interacts with it, this function gets called with all values from the form (i.e. `formValues`). In this case, they are `title` and `description`.

As we mentioned before, you'll get a lot of props from using `reduxForm` component. In `<Field>`, we pass those props to the component `renderInput`, one of them is `meta`, you can find `error` in there. So we can use `meta.error` to print out the error object we defined in advance.
```js
renderInput = ({ input, label, meta }) => {...};

render() {
  return(
    <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
      <Field name="title" component={this.renderInput} label="Enter Title"/>
    </form>
  )
}
```

Notice that **when the form is initially rendered, validate function gets called with all values from the form**. So you'll get an error when initializing, you can make use of that user *touched* or not to tell.
```js
renderError({ error, touched }) {
  if (touched && error) {...}
}

renderInput = ({ input, label, meta }) => {
  const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
  return (
    <div className={className}>
      <label>{label}</label>
      <input {...input} autoComplete="off" />
      {this.renderError(meta)}
    </div>
  );
};
```
Sometimes, you all set up but error message is not shown up, please check you CSS framework. For example, Semantic UI by default is going to hide error messages in some HTML tag, so you need to update `className` like `ui form error` to show it up like so.

If you want to use `connect` component in the class which is using `reduxForm` compoennt, you can use it nestly like so:
```js
const formWrapped = reduxForm({ form: 'streamCreate', validate })(StreamCreate);
export default connect(null, { createStream })(formWrapped);
```

## REST
We need a API server to communicate with client. For example, we can tell users what streaming they can see now. We use [JSON server](https://www.npmjs.com/package/json-server) to provide this backend service.

### JSON Server
To set up a JSON server, the first step is to create a file [db.json](streams/api/db.json) as a database.
```json
"streams": []
```

We can run JSON server by following command (see script in [package.json](streams/api/package.json)):
```
json-server -p 3001 -w db.json
```

We are using JSON server to store data, so we need to think about the data structure. Now we have 2 choices:
```js
// Array-based (bad!)
[{}, {}, {}, ...]

// Object-based (good!)
{1: {}, 2: {}, 3: {}, ...}
```

Object-based structure is better than array-based structure, because we have ID as a key. If we need to update the data, we can find the object by ID instead of comparing whole data.

### CRUD
Now we can handle creating, fetching, updating and deleting, it's so-called *CRUD*(creating, reading, updating, and deleting). First step, we define all types in [actions/types.js](streams/client/src/actions/types.js). Then, we define all actions in [actions/index.js](streams/client/src/actions/index.js). For example, take a look for `createStream` action creator like so:
```js
export const createStream = (formValues) => async (dispatch, getState) => {
  const { userId } = getState().auth;
  const response = await streams.post('/streams', { ...formValues, userId });

  dispatch({ type: CREATE_STREAM, payload: response.data });
  history.push('/');
};
```

We send a request to API server to create a new stream. Then, we dispatch an action to reducer after receiving a response from server. `response.data` is only one record which is this created stream. Let's see how our reducer handles this action (see [streamReducer.js](streams/client/src/reducers/streamReducer.js)):
```js
export default (state = {}, action) => {
  switch (action.type) {
    case CREATE_STREAM:
      return { ...state, [action.payload.id]: action.payload };
  }
};
```

Other operations are similar with `createStream` action creator. But notice that `deleteStream` action creator is kind of a little different. Because we just need an ID to deleted data in our reducer, so we don't need the whole `response.data`. Instead, we only need an ID.

Another difficult part is `fetchStreams` action creator. When we get data from API server, the data is array-based. But we want to store it as object-based in reducer, we can use the following code:
```js
// [{}, {}, ...] -> {1: {}, 2: {}, 3: {}, ...}
{ state..., ..._.mapKeys(action.payload, "id") }
```

Let's see an interesting issue for action `FETCH_STREAM` in [streamReducers.js](streams/client/src/reducers/streamReducer.js). Why do we need to update the state? That's because the state is like the Redux own database, so when we get the data from true API, we have to store it in Redux state. If this stream is existing, we don't care, because the data from API is what we want to use and update to date.

Notice that we only show the data which user ID in stream is equal to current user ID. If we use POST to update data, and we just pass 2 fields, the user ID would be wiped out. That's why we use PATCH to do a partial update.

### Programmatic Navigation
When a user interacts with our app, sometimes we need to navigate this user to another page. For example. when users click **Submit** to create a stream, the page should go back to homepage. We run code to forcibly navigate the user through our app, it's so-called *programmatic navigation*. If user clicks on a `Link` component to navigate, we call *intentional navigation*.

See the code in [actions/index.js](streams/client/src/actions/index.js). After we dispatch data to reducer, we forcibly navigate users to homepage:
```js
export const createStream = (formValues) => async (dispatch, getState) => {
  ...
  dispatch({ type: CREATE_STREAM, payload: response.data });
  history.push('/');
};
```

We discussed `History` object before. It's created by `BrowserRouter`, it keeps track of the address in the address bar of your browser. So anytime the address changes, this object will communicate the change over to the `BrowserRouter`. `History` object also has ability to change the address bar as well. When `BrowserRouter` renders a certain component, it passes the `History` object as a prop down to the component, so the component is easy to trigger some navigation inside of it. 

But the bad news is action creator cannot get reference of `History` object, there are 2 ways to fix it.

One possible solution is to pass `History` object to action creator. But this is not a good way, because every time you want to do programmatic navigation, we have to pass `history` object to action creators:
```js
export const createStream = (formValues, history) => async (dispatch, getState) => {
  ...
  history.push('/');
};
```

The best solution is to create the `History` object by ourself, see [history.js](streams/client/src/history.js). Every time we want to use `History`, we just import it very easily because we are maintaining control over the `History` ourself, and we are not allowing react router to create the `History` itself. Because we created `History` object ourself, we don't need to create a `BrowserRouter`, we just need to create a plain router.
<br/><img src="./screenshots/custom-history.png" width="500"/><br/>

Check the code in [App.js](streams/client/src/components/App.js), see how we use plain `Router`:
 ```js
import { Router } from 'react-router-dom';
import history from '../history';

const App = () => {
  return (
    <div className="ui container">
      <Router history={history}>...</Router>
    </div>
  )
}
```

### URL-based Selection
When a user clicks on a specific stream to edit, we redirect he/she to a page to do some changes. We need to record which stream user is editing now. There are 2 ways to handle:

First solution is to use *selection reducer*. We can use selection reducer to handle edit, delete, or show stream with different ID by passing ID as a variable to this reducer.

Another common solution is to use URL-based selection. We can use URL to tell different user like `/steams/edit/:id` or `/steams/show/:id`. See the code in [StreamList.js](streams/client/src/components/streams/StreamList.js):
```js
renderAdmin(stream) {
  if (stream.userId === this.props.currentUserId) {
    return (
      <div className="right floated content">
        <Link to={`/streams/edit/${stream.id}`}>Edit</Link>
      </div>
    );
  }
}
```
Notice that user navigates to `StreamEdit` component from `StreamList`, it's passed through by `Router` in [App.js](streams/client/src/components/App.js), so all props in `App` component will be down to this component, e.g., `history`.

### Component Isolation
Sometimes we want to navigate to the specific component by typing its URL (e.g., share link or my favorite), but if this component is depend on another component, like some props need to be passed from there, we'll get undefined. **With React Router, each component needs to be designed to work in isolation (i.e., fetch its own data).**

See the code in [StreamEdit.js](streams/client/src/components/streams/StreamEdit.js), we have to fetch stream here to prevent undefined. Notice that this component will be rendered 2 times, first is initialization of this component, in the mean while, `fetchStream()` function calls API to get data. After getting data, this component will be rendered second time, so we need to prevent undefined in first time, we add `if-block` to check `stream` is undefined or not.

```js
class StreamEdit extends Component {
  componentDidMount() {
    this.props.fetchStream(this.props.match.params.id);
  }
  render() {
    if (!this.props.stream) {
      return <div>Loading...</div>;
    }
    ...
  }
}
```

### Code Reuse
It's always a good practice to reuse the code, We did some code reuse in this project. We created [StreamForm.js](streams/client/src/components/streams/StreamForm.js) to reuse the code for `StreamEdit` and `StreamCreate` components.
<br/><img src="./screenshots/stream-form.png" width="450"/><br/>

Passing callback function (e.g. `onSubmit()`) is easy, but how to pass initial values? We have to use special props named `initialValues`, see [StreamEdit](streams/client/src/components/streams/StreamEdit.js). If you use this props, the redux form will receive this props automatically and store in state named `values`. There are 4 fields in `this.props.stream`, which are title, description, ID, and user Id. But ID and user ID should not be updated, so we can pass only 2 fields like so:
```js
<StreamForm
  initialValues={_.pick(this.props.stream, 'title', 'description')}
  onSubmit={this.onSubmit}
/>
```

In `StreamForm`, `<Field>` with a argument `name="title"` will look at any initial values that are passed in, and it will see if that initial values has the property of `title`. If they do then those initial values will be used as the initial values for the field.

## React Portals
In delete stream scenario, we want to add a pop-up for user to confirm. This is so-called *modal window*. The modal is on top of all components, but delete stream comoponent is deep nested in body root. If we want to show this modal, it's not easy to control CSS. So we have to use *React Protals* to let modal is a child of body, not a deep nested under the root.
<br/><img src="./screenshots/portals.png" width="550"/><br/>

There are 3 steps to set up and use React Portals:
1. To use modal, we need to add a new `<div>` in [index.html](streams/client/public/index.html).
    ```html
    <html>
      <body>
        <div id="root"></div>
        <div id="modal"></div>
      </body>
    </html>
    ```
1. Use Portal to render `<body>` element directly, see [Modal](streams/client/src/components/Modal.js) (check modal CSS in Semantic UI [here](https://semantic-ui.com/modules/modal.html)).
    ```js
    const Modal = (props) => {
      return ReactDOM.createPortal(
        <div className="ui dimmer modals visible active">...</div>,
        document.querySelector('#modal')
      );
    };
    ```
    > ### *Note*
    > The most common case in which you will use `createPortal()` is if you are trying to introduce React into a server side rendered (SSR) application.
1. After above 2 steps, we can easily use `Modal` component anywhere we want.

In our case, we put Modal in [StreamDelete.js](streams/client/src/components/streams/StreamDelete.js). Users expect if they click the background then the modal should be gone, so we have to add `onClick` to handle it.
```js
const Modal = props => {
  return ReactDOM.createPortal(
    <div onClick={() => history.push('/')}>
        ...
    </div>, document.querySelector('#modal')
  );
}
```
But now if a user clicks the action button (delete or cancel), the page navigates to home as well. That's because any event in above `<div>` will bubble up until someone handles it. So we add `stopPropagation()` method to prevent.
```js
const Modal = props => {
  return ReactDOM.createPortal(
    <div onClick={() => history.push('/')}>
      <div onClick={e => e.stopPropagation()}>...</div>
    </div>, document.querySelector('#modal')
  );
}
```

## Streaming
The final step is to build our RTMP (Real Time Messaging Protocol) server and stream the videos. 

### RTMP Server
We need a dependency `node-media-server`. Then, create a [index.js](streams/rtmpserver/index.js) and put some default setting in it, you can refer to official [document](https://github.com/illuspas/Node-Media-Server#npm-version-recommended).
```js
const NodeMediaServer = require('node-media-server');

const config = {
  rtmp: {
    port: 1935,
    chunk_size: 60000,
    gop_cache: true,
    ping: 30,
    ping_timeout: 60
  },
  http: {
    port: 8000,
    allow_origin: '*'
  }
};

var nms = new NodeMediaServer(config)
nms.run();
```

You can run RTMP server using following command.
```
node index.js
```

RTMP server is running on port 1935 and 8000. Port 1935 handles the traffic from OBS, and port 8000 serves our client.
<br/><img src="./screenshots/rtmp-server.png" width="650"/><br/>

### OBS
Follow the steps to set up OBS (Open Boardcaster Software):
1. Download OBS from official web [here](https://obsproject.com/).
1. Install it and give OBS right to record your screen. You might need to restart your computer.
    <br/><img src="./screenshots/obs-1.png" width="600"/>
1. Skip auto configuration, click **+** on the bottom to create a new scene named `Streaming Scene`.
    <br/><img src="./screenshots/obs-2.png" width="250"/>
1. Go to **Sources** section, click **+** and select **Display Capture**.
    <br/><img src="./screenshots/obs-3.png" width="300"/>
    <br/><img src="./screenshots/obs-4.png" width="700"/>
1. You can adjust the preview to fit your screen by moving red point as below. This preview is determined by the setting of resolution.
    <br/><img src="./screenshots/obs-5.png" width="1400"/>
1. Go to **Sources** section, click **+** and select **Audio Input Capture**. Then, choose your input source.
    <br/><img src="./screenshots/obs-6.png" width="700"/>
1. You can test the display and audio source by click **Start Recording** in the **Controls** section. Then you can click **Settings** and find your output default folder.
    <br/><img src="./screenshots/obs-7.png" width="950"/>

### Live Stream
There are many different formats which RTMP supports, see [here](https://github.com/illuspas/Node-Media-Server#accessing-the-live-stream). HLS and DASH are popular types of video formats that are used for streaming, but you need some tricky setting on the machine. So we use http-flv in this app. This is a Flash video format. To use it, you can refer to official [example](https://www.npmjs.com/package/flv.js#getting-started) or RTMP server [example](https://github.com/illuspas/Node-Media-Server#via-flvjs-over-http-flv).

We need to get a reference to the video element and then we will create a player and pass the reference to the video element off to that player.
```js
import flv from 'flv.js'

class StreamShow extends Component {
  constructor(props) {
    super(props);
    this.videoRef = React.createRef();
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetchStream(id);
    this.buildPlayer();
  }

  buildPlayer() {
    // If we already set up the player, we don't set up again
    // If stream is not rendered yet, we do nothing.
    if (this.player || !this.props.stream) {
      return;
    }
    const { id } = this.props.match.params;

    // flv settings
    this.player = flv.createPlayer({
      type: 'flv',
      url: `http://localhost:8000/live/${id}.flv`,
    });

    this.player.attachMediaElement(this.videoRef.current);
    this.player.load();
    // We don't want to play the video automatically
    // this.player.play();
  }

  render() {
    return (
      <div>
        <video ref={this.videoRef} style={{ width: "100%" }} controls />
      </div>
    );
  }
}
```
Notice that first time we visit `StreamShow` component, `fetchStream()` function needs some time to get response, so `this.videoRef.current` is `null`. To avoid rendering failed, we've added `if-statement`. 

Notice that when user leave this component, the streaming keeps going. Client still downloads the data from RTMP server, so we need to stop streaming.
```js
componentWillUnmount() {
  this.player.destroy();
}
```
