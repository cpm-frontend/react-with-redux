# React with Redux
This is a note for learning *React* and *Redux*.

There are 4 parts of this note:
1. [React](react/): We will learn some basics of *React* and how to build frontend apps by *React*.
1. [React Hooks](react-hooks/): We are going to learn how to use *hooks system* in a function component.
1. [Redux](redux/): We will introduce how to use *Redux* to control states.
1. [Context System](context-system/): In this section, we will introduce context system and compare it with *Redux*.

## Reference
[Modern React with Redux](https://www.udemy.com/react-redux/).