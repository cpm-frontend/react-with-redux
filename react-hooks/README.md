# React Hooks

*Hooks system* is all about giving function components a lot of additional functionality. Hooks are a way to write reusable code, instead of more classic techniques like inheritance. With hooks system, React give us functions like:
- `useState()`: It lets you use *state* in a functional component.
- `useEffect()`: It lets you use something like *lifecycle methods* in a functional component.
- `useRef()`: It lets you create a *ref* in a function component.

There are 10 functions (`useState()`, `useEffect()`, `useContext()`, `useReducer()`, `useCallback()`, `useMemo()`, `useRef()`, `useImperativeHandle()`, `useLayoutEffect()` and `useDebugValue()`) that are included with React to give more functionality to function components. We will make use of these hooks to write our own custom hooks.

You can refer to the [widgets](widgets/) project to learn following concepts (section [Function useState](./README.md#function-usestate), [Function useEffect](./README.md#function-useeffect), and [Function useRef](./README.md#function-useref)). Follow the steps: 

1. Install packages.
    ```
    cd widgets
    yarn
    ```
1. Copy the following key, which is used for Google translate APIs. Notice that this is a paid API only for *localhost:3000* domain.
    ```
    AIzaSyCHUCmpR7cT_yDFHC98CZJy2LTms-IwDlM
    ```
1. Go to [Convert.js](widgets/src/components/Convert.js) and replace `<GOOGLE_TRANSLATE_API_KEY>` with above key.
1. Type the following commands to run service:
    ```
    yarn start
    ```

## Function `useState()`
`useState()` function lets you use *state* in a functional component. Let's compare *state* in a class component and a function component like so (they are same):
```js
// Class component
class Accordion extends React.Component {
  state = { activeIndex: null };

  onTitleClick = (index) => {
    this.setState({ activeIndex: index });
  };

  renderedItems = this.props.items.map((item, index) => {...});

  render() {
    return (
      <div className="ui styled accordion">
        {this.renderedItems}
        <h1>{this.state.activeIndex}</h1>
      </div>
    );
  }
}
```
```js
// Function component
const Accordion = ({ items }) => {
  const [activeIndex, setActiveIndex] = useState(null);
  
  const onTitleClick = (index) => {
    setActiveIndex(index);
  };

  const renderedItems = items.map((item, index) => {...});

  return (
    <div className="ui styled accordion">
      {renderedItems}
      <h1>{activeIndex}</h1>
    </div>
  );
};
```

For the syntax in function component here:
```js
const [activeIndex, setActiveIndex] = useState(null);
```

There is no array created, this is short for destructuring, same as below:
```js
const thing = useState(null);
const activeIndex = thing[0];
const setActiveIndex = thing[1];
```

We give `useState()` a argument as initial value for this piece of state. First element we get (e.g. `activeIndex`) is this piece of state, second element we get (e.g. `setActiveIndex`) is a **function** to change this piece of state (so-called setter).

Notice that every time we call this setter, this function component is rendered (like we call `setState()`).

See following comparison:

--|Class Components|Function Components
--|--|--
Initialization|`state = { activeIndex: 0 }`|`useState(0)`
Reference|`this.state.activeIndex`|`activeIndex`
Updates|`this.setState({ activeIndex: 10 })`|`setActiveIndex(10)`

> ### *Note*
> Class component can easily define or update multiple state in the same time, but function component need to call `useState()` several time to define and update.

## Function `useEffect()`
`useEffect()` allows function components to use something like *lifecycle methods*. We configure the hook to run some code automatically in one of three scenarios:
1. When the component is rendered for the first time only.
1. When the component is rendered for the first time and whenever it rerenders.
1. When the component is rendered for the first time, and whenever it rerenders and some piece of data has changed 

We can use second argument in `useEffect` corresponding to above 3 scenarios:
```js
// Scenario 1:
// Run at initial render
useEffect(() => {...}, []);

// Scenario 2:
// Run at initial render and after every rerender
useEffect(() => {...});

// Scenario 3:
// Run at initial render and after every rerender if 'term' or 'name' has changed since last render
useEffect(() => {...}, [term, name]);
```

### Async in `useEffect()`
You are not allowed to use async function directly or return a `Promise` in `useEffect()` function. See following code will result in error:
```js
useEffect(async () => {
  await axios.get(...);
});
```

There are 3 ways to fix this issue:
1. (Recommended) Write the async function inside the effect and call it immediately.
    ```js
    useEffect(() => {
      const search = async () => {
        await axios.get(...);
      };
      search();
    });
    ```
1. It's same with first one but using another syntax.
    ```js
    useEffect(() => {
      (async () => {
        await axios.get(...);
      })();
    });
    ```
1. Use Promises chain.
    ```js
    useEffect(() => {
      axios.get(...).then(...);
    });
    ```

### XSS Attacks
In this application, we want to use `snippet` data from wiki, it's HTML content like:
```html
also published <span class="searchmatch">Programming</span> Languages: History and Fundamentals, which went on to be a standard work on <span class="searchmatch">programming</span> languages. <span class="searchmatch">Programs</span> were mostly still...
```

You might think we can use this data as JSX to create a HTML element. In other word, we want to turn a string into JSX. If we want to do this, we can use following React feature:
```js
// DON'T DO THIS!
<span dangerouslySetInnerHTML={{ __html: result.snippet }}></span>
```

Notice that the above method is very dangerous, hacker can do *XSS attacks* by this way, so ***DON'T DO THAT***! XSS attack stands for cross-site scripting attack, that is where we accidentally pick up and render some HTML from an untrusted source that can allow some hacker to execute some JS code inside of our application.

### Throttling API Requests
See the following code, if we type something in input, the `term` will be updated immediately. After term is updated, it will send a request. This is not efficiency, for example, if we want to search a word 'train', then the term will be updated 5 times, so we will send 5 requests to wiki.
```js
const Search = () => {
  const [term, setTerm] = useState('');
  useEffect(() => {
    const search = async () => {
      const { data } = await axios.get('https://en.wikipedia.org/w/api.php');
      setResults(data.query.search);
    };
  }, [term]);

  return (
    <div>
      <input
        className="input"
        value={term}
        onChange={(e) => setTerm(e.target.value)}
      ></input>
    </div>
  );
}
```

To fix this issue, we can set a timer to search. For example, we can wait for 0.5 second, if there are no additional changes then we send a request. First of all, let's see how to use `setTimeout()` and `clearTimeout()`. Go to your browser console and try following code.
```js
// Function will be called after 10 seconds, it will return timer ID
> setTimeout(() => console.log('Hi'), 10000);
36

// We can cancel timer by ID before time's up, otherwise, 'Hi' will be printed out
> clearTimeout(36)
```

We can store the previous state to detect the term is changed again, if so, we reset the timer. For this purpose, we can use another feature of `useEffect()`.

The only thing `useEffect()` can return is another function like so:
```js
useEffect(() => {
  console.log('Initial render or term was changed');
  return () => {
    console.log('CLEANUP');
  };
}, [term]);
```

This special returned function will be called after next rerender and before `useEffect()` is called. See following figure:
<br/><img src="./screenshots/useeffect.png" width="450"/><br/>

In above example, you will see console log like:
```js
// First render
Initial render or term was changed

// Rerender
CLEANUP
Initial render or term was changed

// Rerender again
CLEANUP
Initial render or term was changed
```

So we can use this returned function to clean up our timer.
```js
const Search = () => {
  const [term, setTerm] = useState('');
  useEffect(() => {
    const search = async () => {...};

    // We don't want to wait for first render
    if (term && !results.length) {
      search();
    } else {
      const timeoutId = setTimeout(() => {
        if (term) {
          search();
        }
      }, 500);

      return () => {
        clearTimeout(timeoutId);
      };
    }
  }, [term]);
}
```

### Debounce (*Advanced*)
The content in this paragraph is very advanced, it's optional. We will fix warning in console and improve API request efficiency. See the following code (it's some part of previous example):
```js
const Search = () => {
  useEffect(() => {
    // We don't want to wait for first render
    if (term && !results.length) {
      ...
    } else {
      ...
    }
  }, [term]);
}
```

Anytime that you refer to a prop or a piece of state inside of `useEffect()` (e.g. `term` and `results.length`), React or some rule built-in *create-react-app* or *ESLint* is going to want you to reference it. That's why you will see the warning in your browser console like so:
<br/><img src="./screenshots/warning.png" width="400"/><br/>

If we don't reference it, this code can lead to some really weird and hard to debug problems. It's easy to fix this issue by following the hint in the warning, so we add `results.length` in dependency array like so:
```js
useEffect(() => {
  // We don't want to wait for first render
  if (term && !results.length) {
    ...
  } else {
    ...
  }
}, [term, results.length]);
```

But when we go back to browser again, you notice that we send 2 requests in first render, why? Because now we reference `term` and `results.length`, after first render, the value of `results.length` is changed from 0 to 10, so React will call render again and send second request.

To fix warning issue, we introduce another bug, that's definitely bad. To fix all of them, we need to use *debounce* skill:
1. We introduce a new state called `debouncedTerm` which has same default value with `term`. In initial time, component fetches data first time.
1. When user types something, `term` is immediately updated. We set a timer to update `debouncedTerm`. If `term` is not changed after 0.5 second then we update `debouncedTerm`.
1. When `debouncedTerm` is updated, we fetch data again.

The following code implements above steps:
```js
const Search = () => {
  const [term, setTerm] = useState('programming');
  const [debouncedTerm, setDebouncedTerm] = useState(term);
  const [results, setResults] = useState([]);

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      if (term) {
        setDebouncedTerm(term);
      }
    }, 500);

    return () => {
      clearTimeout(timeoutId);
    };
  }, [term]);

  useEffect(() => {
    const search = async () => {
      const { data } = await axios.get('https://en.wikipedia.org/w/api.php', {
        params: {
          action: 'query',
          list: 'search',
          origin: '*',
          format: 'json',
          // We use 'debouncedTerm' to query data
          srsearch: debouncedTerm,
        },
      });
      setResults(data.query.search);
    };
    search();
  }, [debouncedTerm]);
}
```

Now our app only send a request in first time and no warning in our browser console. Another benefit of this method is when user type same word quickly, our app doesn't send a request again. Because the word user types only changes `term`, if user types a same word, `debouncedTerm` is not changed, so our app won't send another request to wiki.

## Function `useRef()`
We add a new feature which is dropdown list. See the following code:
```js
const Dropdown = ({ options, selected, onSelectedChange }) => {
  const [open, setOpen] = useState(false);

  const renderedOptions = options.map((option) => {
    return (
      <div onClick={() => onSelectedChange(option)}>
        {option.label}
      </div>
    );
  });

  return (
    <div className="ui form">
      <label className="label">Select a Color</label>
      <div
        onClick={() => setOpen(!open)}
        className={`ui selection dropdown ${open ? 'visible active' : ''}`}
      >
        <i className="dropdown icon"></i>
        <div className="text">{selected.label}</div>
        <div className={`menu ${open ? 'visible transition' : ''}`}>
          {renderedOptions}
        </div>
      </div>
    </div>
  );
};
```

When we click dropdown list, the list is open. If you select one of them then list is closed. Because we use `onClick` to listen and use `setOpen()` to toggle. The issue is when we click outside of this list, we expect the dropdown list should be closed, but it's not. Because we don't listen this kind of event. The challenge is here, it seems that it's not easy to listen events that are outside of this dropdown element.

### Event Bubbling
Let's see above code example again. When user clicks some item in dropdown list, which means user triggers a click event. This event will bubble up to whole HTML. In this case, `onClick` in `renderedOptions` will be called first, so it will call `onSelectedChange()` method.

Notice that this event does not stop here, it will bubble up until top level. So the next `onClick` in `div.ui.selection` element is triggered to call `setOpen()` function.

To fix above issue, the dropdown can set up a manual event listener (whithout React) on the *body* element. Then all events on the *body* element can trigger the function to close the dropdown list like so:
```js
useEffect(() => {
  document.body.addEventListener(
    'click',
    () => {
      setOpen(false);
    },
    { capture: true }
  );
}, []);
```

But this solution will introduce another bug. Now there are 3 places with listener to listen *onClick* event, which is body listener, item listener and dropdown listener. We expect the event should bubble up, so the order is item first, then dropdown, finally body. But it's not.

Body listener is wired up with a manual listener, but item and dropdown event handlers (listeners) are wired up through React. **Listeners that are wired up using `addEventListener()` actually get called first, after all of those are called then and only then do all of React event listeners get called.**
<br/><img src="./screenshots/bubble-up.png" width="400"/><br/>

So when we click outside of dropdown, then dropdown list is closed correctly. But when we select any item inside of dropdown, the list won't be closed. Because body listener is triggered first, now the state of `open` is `false`, then item listener is triggered, finally the dropdown listener is triggered to turn `open` to `true`. That's a bug we were discussing.

### Referencing to DOM
Our strategy is to separate to 2 scenarios to fix, see following discussion:
- When user clicks on an element that is created by the `Dropdown` component, we don't want the *body* event listener to do anything.
    <br/><img src="./screenshots/scenario-1.png" width="450"/>
- When user clicks on any element besides the ones created by the `Dropdown` component, we want the *body* event listener to close the dropdown list.
    <br/><img src="./screenshots/scenario-2.png" width="600"/>

We can use following code to get where event happened:
```js
document.body.addEventListener(
  'click',
  (event) => {
    console.log(event.target);
  },
  { capture: true }
);
```

Now we can use `useRef()` to reference the dropdown element:
```js
const Dropdown = (...) => {
  const ref = useRef();
  return (
      <div ref={ref} className="ui form">...</div>
  );
}
```

Use `ref.current` to get reference as benchmark to compare with the element user clicked on by using `contains()` method.
```js
useEffect(() => {
  document.body.addEventListener(
    'click',
    (event) => {
      if (ref.current && ref.current.contains(event.target)) {
        return;
      }
      setOpen(false);
    },
    { capture: true }
  );
}, []);
```

There is a good practice to clean up listener to avoid other bugs, because the listener we set up is global. For example, if the component outside of `Dropdown` component (e.g. `App`) rerender to `Dropdown`, listener still works, but `ref` might not get a specific DOM element because it's not found. This can let some null pointer issue.

So we can add a return function in `useEffect` to clean up after rerendering like:
```js
useEffect(() => {
  const onBodyClick = (event) => {
    if (ref.current && ref.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  document.body.addEventListener('click', onBodyClick, { capture: true });

  return () => {
    document.body.removeEventListener('click', onBodyClick);
  };
}, []);
```

## Navigation

*Navigation* means showing different sets of components when the URL changes. The majority of React apps use *React-router* library. However, React-router has frequently breaking changes. In this section, we will learn the basics of route and how to create a route manually.

We can get URL information by using following code in your browser console:
```js
window.location
```

So we can implement route manually like so:
```js
// App.js
export default () => {
  const [selected, setSelected] = useState(options[0]);

  return (
    <div>
      <Header />
      <Route path="/">
        <Accordion items={items} />
      </Route>
      <Route path="/list">
        <Search />
      </Route>
      <Route path="/dropdown">
        <Dropdown
          label="Select a Color"
          options={options}
          selected={selected}
          onSelectedChange={setSelected}
        />
      </Route>
      <Route path="/translate">
        <Translate />
      </Route>
    </div>
  );
};
```
```js
// Route.js
const Route = ({ path, children }) => {
  return window.location.pathname === path ? children : null;
};
```
```js
// Header.js
const Header = () => {
  return (
    <div className="ui secondary pointing menu">
      <a href="/" className="item">
        Accordion
      </a>
      <a href="/search" className="item">
        Search
      </a>
      <a href="/dropdown" className="item">
        Dropdown
      </a>
      <a href="/translate" className="item">
        Translate
      </a>
    </div>
  );
};
```

But the above method is not ideal, because when we click an item in the header, the browser reloads whole page. This is not good for single page application (SPA). 

Ideally, when user clicks on an item, we should change URL but don't do a full page refresh. Each route could detect the URL has changed and could update piece of state tracking the current `pathname`. Finally, each route rerenders, showing or hiding components appropriately.

### Navigation Events
<img src="./screenshots/navigation.png" width="600"/><br/>
Our solution is to create a `Link` component. In this component, we don't allow the app to refresh whole page, and update the URL. Finally, `Link` sends a navigation event to route.

```js
const Link = ({ className, href, children }) => {
  const onClick = (event) => {
    // When user click on command key on MacOS or ctrl key on Windows, we create a new tab
    if (event.metaKey || event.crtlKey) {
      return;
    }
    // Do not refresh whole page
    event.preventDefault();
    // Update URL
    window.history.pushState({}, '', href);

    // Send a navigation event to Route
    const navEvent = new PopStateEvent('popstate');
    window.dispatchEvent(navEvent);
  };

  return (
    <a onClick={onClick} className={className} href={href}>
      {children}
    </a>
  );
};
```

In route, we listen this navigation event to choose which component should be rendered.
```js
const Route = ({ path, children }) => {
  const [currentPath, setCurrentPath] = useState(window.location.pathname);

  useEffect(() => {
    const onLocationChange = () => {
      setCurrentPath(window.location.pathname);
    };

    // Listen navigation event from Link
    window.addEventListener('popstate', onLocationChange);
    return () => {
      // Clean up listener
      window.removeEventListener('popstate', onLocationChange);
    };
  }, []);

  return currentPath === path ? children : null;
};
```

In header, we can substitute Link component for `<a/>` tag.
```js
const Header = () => {
  return (
    <div className="ui secondary pointing menu">
      <Link href="/" className="item">
        Accordion
      </Link>
      <Link href="/list" className="item">
        Search
      </Link>
      <Link href="/dropdown" className="item">
        Dropdown
      </Link>
      <Link href="/translate" className="item">
        Translate
      </Link>
    </div>
  );
};
```

## Videos App

We are going to use hooks to refactor [react/videos](../react/videos/) app we built in React tutorial. You can refer to the [videos-hooks](videos-hooks/) project, and follow the steps [here](../react/README.md#videos-app) to set up.

### Removing a Callback
When we want to pass a function with one argument like so:
```js
return (
  <VideoList
    onVideoSelect={(video) => setSelectedVideo(video)}
    videos={videos}
  />
)
```

We can use following syntax, it's totally same with above syntax.
```js
return (
  <VideoList
    onVideoSelect={setSelectedVideo}
    videos={videos}
  />
)
```

### Custom Hooks
Custom hook is a best way to create reusable code in a React project (besides components). It can be created by extracting hook-related code out of a function component. Custom hooks always make use of at least one primitive hook internally, each custom hook should have one purpose. Data fetching is a great thing to try to make reusable.

Here are process for creating reusable hooks:
1. Identify each line of code related to some single purpose.
1. Identify the inputs to that code.
1. Identify the outputs to that code.
1. Extract all of the code int oa separate function, receiving the inputs as arguments, and returning the outputs.

For example, see the following code, we can separate it into 2 different purposes.
```js
const App = () => {
  const [videos, setVideos] = useState([]); // videos
  const [selectedVideo, setSelectedVideo] = useState(null); // selectedVideo

  // videos
  useEffect(() => {
    onTermSubmit('buildings');
  }, []);

  // videos
  const onTermSubmit = async (term) => {
    const response = await youtube.get('/search', {
      params: {
        q: term,
      },
    });

    setVideos(response.data.items);
    // The below line need to be handled, it's belong to selectedVideo
    setSelectedVideo(response.data.items[0]);
  };

  // We don't care about JSX in custom hook
  return (...);
};
```

For all stuffs related to videos, we can define the input of this custom hook. All we need to provide is only default value to search, so the input should be `defaultSearchTerm`. The outputs are a list of videos and a way to search for videos. You can return array as React hooks convention or object as JS convention, it's up to you. See [useVideos](videos-hooks/src/hooks/useVideos.js):
```js
const useVideos = (defaultSearchTerm) => {
  const [videos, setVideos] = useState([]);

  useEffect(() => {
    search(defaultSearchTerm);
  }, [defaultSearchTerm]);

  const search = async (term) => {
    const response = await youtube.get('/search', {
      params: {
        q: term,
      },
    });

    setVideos(response.data.items);
  };

  // You can use [] or {} to return
  return [videos, search];
};
```

Finally, we can update our `App` like so:
```js
const App = () => {
  const [selectedVideo, setSelectedVideo] = useState(null);
  const [videos, search] = useVideos('buildings');

  // Take care for handling below logic when refactoring
  useEffect(() => {
    setSelectedVideo(videos[0]);
  }, [videos]);

  return (...);
};
```

## Deployment
When we use create-react-app to build our project, it can help us create some deployment bundle like `index.js`, `bundle.js`, some images and some CSS.

We have been running React app in development mode on local machine, which means that there is a running development server. When we build our project and build up this deployment bundle, we no longer have a running server.

So we have to take those files and upload them to some *deployment target*. This deployment target will take those files and host them. 

Whenever user types in our domain into their browser, their browser will make a request over to our deployment provider and they will immediately get back response that contains always the `index.html` file. That file will be loaded up inside the browser, and inside of it will be some variety of script tags, linked tags and so on. That will cause the browser to make additional requests and get some additional JS files or CSS files.
<br/><img src="./screenshots/client.png" width="700"/><br/>

We do not run some virtual machine to deploy React application. You only need virtual machine if you are running some kind of active server.
<br/><img src="./screenshots/server.png" width="650"/><br/>

We are going to deploy [videos-hooks](videos-hooks/) app in following sections.

### Vercel
*Vercel* is very easy and quick to deploy apps. Follow the steps to deploy:
1. Sign up at [Vercel](https://vercel.com/signup).
1. Install the *Vercel* CLI.
    ```
    cd videos-hooks
    npm install -g vercel
    ```
1. Login *Vercel* in your terminal. You need to verify email address.
    ```
    vercel login
    ```
1. Run command to deploy, just hit enter for all questions. You can see your app link in the console.
    ```
    vercel
    ```
    <img src="./screenshots/vercel.png" width="800"/>
1. If you want to change the code and redeploy. You can type following command (don't need to git commit):
    ```
    vercel --prod
    ```

### Netlify
*Netlify* is a little bit harder to use, but it's also got many features tied to it. Follow the steps to deploy:
1. Create a *Github* repo for your project.
1. Commit changes to your project locally.
1. Link your project to the new repo.
    ```
    git remote add origin git@github.com:<YOUR_ACCOUNT>/<YOUR_REPO_NAME>.git
    ```
1. Push your code to *Github*.
    ```
    git push origin master
    ```
1. Sign up for an account at [Netlify](https://www.netlify.com/).
1. Sign in *Netlify* and click **New site from Git**.
1. Click on Github and select **Only select repositories** to choose your repo, then click on **Install** in the bottom.
    <br/><img src="./screenshots/netlify.png" width="500"/>
1. Click **Deploy site**.
1. Wait a moment, then you can see your web app link.
1. If you want to change the code and redeploy, you can commit these changes and push to *Github*.
    ```
    git add .
    git commit -m "Update something"
    git push origin master
    ```
1. *Netlify* will detect the change automatically and redeploy your project. You can go back to dashboard and see the website link after it finished.
