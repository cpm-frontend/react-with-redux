import React from 'react';

const Link = ({ className, href, children }) => {
  const onClick = (event) => {
    if (event.metaKey || event.crtlKey) {
      return;
    }
    // Do not refresh whole page
    event.preventDefault();
    // Update URL
    window.history.pushState({}, '', href);

    // Send a navigation event to Route
    const navEvent = new PopStateEvent('popstate');
    window.dispatchEvent(navEvent);
  };

  return (
    <a onClick={onClick} className={className} href={href}>
      {children}
    </a>
  );
};

export default Link;
